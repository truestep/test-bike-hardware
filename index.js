import express from 'express'
import bodyParser from 'body-parser'
import api from './api/routes.js'
import https from 'https'
import fs from 'fs'
import cookieParser from "cookie-parser"
const app = express()

app.use(bodyParser.json({limit: '55mb'}))
app.use(cookieParser())


app.use(function (req, res, next) {
    console.log(`req: ${req.method} ${req.originalUrl}`, req.body ? JSON.stringify(req.body) : req.body)
    if (req.method === 'OPTIONS') {
        console.log('OPTIONS header')
        res.setHeader("Access-Control-Allow-Origin", req.headers.origin)
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, OPTIONS")
        res.setHeader("Access-Control-Allow-Credentials", "true")
        res.setHeader("Access-Control-Max-Age", '86400') // 24 hours
        res.setHeader("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, surveyid, x-requestid")
        res.send()
    } else {
        if (req.headers.origin) {
            res.setHeader("Access-Control-Allow-Origin", req.headers.origin) //TODO don't forget about url here
        }
        res.setHeader("Access-Control-Allow-Credentials", "true")
        next()
    }
})

function nocache(req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate')
    res.header('Expires', '-1')
    res.header('Pragma', 'no-cache')
    next()
}

app.use(nocache)

app.use("/api", api)
// catch 404 and forward to error handler


app.use((req, res, next) => {
    console.error("Not Found", req.url, req.originalUrl)
    const err = new Error('Not Found')
    err.status = 404
    next(err)
})

// error handler
app.use((err, req, res, next) => {
    console.error("ERROR!", err)
    // set locals, only providing error in development
    const error = {
        message: err.message,
        err: err
    }
    // render the error page
    res.status(err.status || 500)
    res.end(JSON.stringify(error))
})

// const port = process.env.PORT || 4000
// const httpServer = app.listen(port, () => console.log('http app listening on port ', port))
// httpServer .headersTimeout = 30 * 60 * 1000
global.__basedir = process.cwd()

const httpsServer = https.createServer({
        key: fs.readFileSync(global.__basedir + '/certs/server.key'),
        cert: fs.readFileSync(global.__basedir + '/certs/server.cert')
    },
    app)
httpsServer.headersTimeout = 30 * 60 * 1000
// httpsServer.setTimeout(30 * 60 * 1000, (err) => {
// console.log("timeout ", err)
// })
const httpsPort = process.argv[2] || 8443
httpsServer.listen(httpsPort, () => console.log(`Https app listening on port ${httpsPort}`))

process
    .on('unhandledRejection', (reason, p) => {
        console.error(reason, 'Unhandled Rejection at Promise', p)
        process.exit(1)
    })
    .on('uncaughtException', err => {
        console.error(err, 'Uncaught Exception thrown')
        process.exit(1)
    })
