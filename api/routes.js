'use strict'
import express from 'express'
const api = express.Router()

import bicycle from './bicycle.js'

api.post('/id', bicycle.setId)
api.post('/light', bicycle.setLight)
api.post('/status', bicycle.setId)
api.post('/location', bicycle.setLocation)
api.post('/getip', bicycle.getIp)
api.post('/locked', bicycle.setLock)
api.post('/getstate', bicycle.getState)


export default api
