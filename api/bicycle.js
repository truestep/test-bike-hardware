'use strict'

class BicycleApi {
    constructor(id) {
        for (let obj = this; obj; obj = Object.getPrototypeOf(obj)){
            for (let name of Object.getOwnPropertyNames(obj)){
                if (typeof this[name] === 'function'){
                    this[name] = this[name].bind(this)
                }
            }
        }
        this.id = id
    }

    id
    light
    status
    location
    ip
    lock


    setId(req, res) {
        try {
            this.id = req.body.id
            res.send()
        } catch (e) {
            console.error('status error ', e)
            res.status(500)
            res.send(e)
        }
    }

    setLight(req, res) {
        this.light= req.body.state
        res.send()
    }

    setStatus(req, res) {
        this.status = req.body.status
        res.send()
    }

    setLocation(req, res) {
        this.location = req.body.location
        res.send()
    }

    getIp(req, res) {
        this.ip = req.body.ip
        res.send()
    }

    setLock(req, res) {
        this.lock = req.body.state
        res.send()
    }

    getState(req, res) {
        res.send(
        {
            id:this.id,
            light:this.light,
            status:this.status,
            location:this.location,
            ip:this.ip,
            lock:this.lock,
        })
    }

}

const bicycleApi = new BicycleApi(process.argv[2])

export default bicycleApi